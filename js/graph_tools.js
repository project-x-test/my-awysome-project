
//делаем заливку канваса
  function drawFace(ctx, width, height) {
    // Create gradient
    var grd=ctx.createLinearGradient(0,300,0,0);
    // grd.addColorStop(0,"MidnightBlue");
    // grd.addColorStop(1,"Blue");

    grd.addColorStop(0,"rgba(0,0,0,0.1)");
    grd.addColorStop(1,"rgba(0,0,0,0.5)");


   // Fill with gradient
   ctx.fillStyle=grd;
   ctx.fillRect(0,0,width,height);

   // ctx.fillStyle = "#FF0000";
   // ctx.fillRect(0,0,150,75);
}

//рисуем линию в декартовых координатах от (0,0) верхнего левого угла контента канваса
function drawLineDec(ctx, pos_x1, pos_y1, pos_x2, pos_y2, width, color) {
  ctx.beginPath();
  ctx.lineWidth = width;
  ctx.strokeStyle=color;
  ctx.lineCap = "round";

  ctx.moveTo(pos_x1,pos_y1);
  ctx.lineTo(pos_x2, pos_y2);
  ctx.stroke();
}

//рисуем линию в полярных координатах, угол в градусах от оси Х по часовой
function drawLineAng(ctx, pos_x, pos_y, ang, length, width, color) {
    ctx.lineWidth = width;
    ctx.strokeStyle=color;
    ctx.lineCap = "round";

    ctx.translate(pos_x, pos_y); // переносим точку отсчета системы координат канваса
    ctx.rotate(ang*Math.PI/180.0); //поворачиваем на угол ang по часовой отн. Х в т. (0,0) новой системы
    ctx.beginPath();
    ctx.moveTo(0, 0);
    ctx.lineTo(length,  0); //рисуем линию длины length вдоль оси X повернутой системы
    ctx.stroke();
    ctx.setTransform(1, 0, 0, 1, 0, 0);// возвращаем систему координат в начальное попложение
 }

//рисуем стрелку из первой координаты во вторую
function drawArrow(ctx, pos_x1, pos_y1, pos_x2, pos_y2, width, color){
  drawLineDec(ctx, pos_x1, pos_y1, pos_x2, pos_y2, width, color);
  let sin_alpha=(pos_x2-pos_x1)/Math.sqrt((pos_x2-pos_x1)*(pos_x2-pos_x1)+(pos_y2-pos_y1)*(pos_y2-pos_y1));
  let alpha=(Math.asin(sin_alpha)+Math.PI/2)*180.0/Math.PI;// угол отн. оси X в градусах
  if (pos_y2>pos_y1) alpha=-alpha;
  drawLineAng(ctx, pos_x2, pos_y2, alpha+20, 20, width, color);
  drawLineAng(ctx, pos_x2, pos_y2, alpha-20, 20, width, color);
}

//рисуем двустороннюю стрелку между первой и второй координатами
function drawArrowD(ctx, pos_x1, pos_y1, pos_x2, pos_y2, width, color){
  drawLineDec(ctx, pos_x1, pos_y1, pos_x2, pos_y2, width, color);
  let sin_alpha=(pos_x2-pos_x1)/Math.sqrt((pos_x2-pos_x1)*(pos_x2-pos_x1)+(pos_y2-pos_y1)*(pos_y2-pos_y1));
  let alpha=(Math.asin(sin_alpha)+Math.PI/2)*180.0/Math.PI;// угол отн. оси X в градусах
  if (pos_y2>pos_y1) alpha=-alpha;
  drawLineAng(ctx, pos_x2, pos_y2, alpha+20, 20, width, color);
  drawLineAng(ctx, pos_x2, pos_y2, alpha-20, 20, width, color);
  alpha=alpha+180;
  drawLineAng(ctx, pos_x1, pos_y1, alpha+20, 20, width, color);
  drawLineAng(ctx, pos_x1, pos_y1, alpha-20, 20, width, color);
}

//рисуем размер между двумя точками
function DimH(ctx, pos_x1, pos_y1, pos_x2, pos_y2, width, color, dim_in_out, value){
  let nArrow=60; //отступ двусторонней стрелки размера
  let nLine=70; //длина размерных линий
  let cos_alpha=(pos_y2-pos_y1)/Math.sqrt((pos_x2-pos_x1)*(pos_x2-pos_x1)+(pos_y2-pos_y1)*(pos_y2-pos_y1));
  let sin_alpha=(pos_x2-pos_x1)/Math.sqrt((pos_x2-pos_x1)*(pos_x2-pos_x1)+(pos_y2-pos_y1)*(pos_y2-pos_y1));
  let alpha=Math.asin(sin_alpha);
  if (pos_y2>pos_y1) alpha=-alpha;

  //alert("dim_in_out="+dim_in_out);

  if(dim_in_out=="in")
  {
    x1_A=pos_x1-nArrow*cos_alpha;
    x2_A=pos_x2-nArrow*cos_alpha;
    y1_A=pos_y1+nArrow*sin_alpha;
    y2_A=pos_y2+nArrow*sin_alpha;

    x1_L=pos_x1-nLine*cos_alpha;
    x2_L=pos_x2-nLine*cos_alpha;
    y1_L=pos_y1+nLine*sin_alpha;
    y2_L=pos_y2+nLine*sin_alpha;
  }else
  {
    x1_A=pos_x1+nArrow*cos_alpha;
    x2_A=pos_x2+nArrow*cos_alpha;
    y1_A=pos_y1-nArrow*sin_alpha;
    y2_A=pos_y2-nArrow*sin_alpha;

    x1_L=pos_x1+nLine*cos_alpha;
    x2_L=pos_x2+nLine*cos_alpha;
    y1_L=pos_y1-nLine*sin_alpha;
    y2_L=pos_y2-nLine*sin_alpha;
  }

  drawArrowD(ctx, x1_A, y1_A, x2_A, y2_A, width, color);//двусторонняя стрелка между параллельно смещенными точками размеров
  drawLineDec(ctx, pos_x1, pos_y1, x1_L, y1_L, width, color)//линия первого размера
  drawLineDec(ctx, pos_x2, pos_y2, x2_L, y2_L, width, color)//линия второго размера

  if (value==null) {value=Math.sqrt((pos_x2-pos_x1)*(pos_x2-pos_x1)+(pos_y2-pos_y1)*(pos_y2-pos_y1)).toFixed(0);}

  ctx.font="50px Arial";
  ctx.fillStyle = color;
  drawLabelAtAngle(value,(x1_A+x2_A)/2,(y1_A+y2_A)/2,alpha+Math.PI/2,30);
}

function drawLabelAtAngle(text,x,y,angle,offset){

    // calc the angle clampled between 0 & PI*2
    var normA=(angle+Math.PI*2)%(Math.PI*2);

    // get ready to flip the text if it is on the left side
    if(normA>Math.PI*3/2 || normA<Math.PI/2){
        ctx.textAlign='left';
        var flip=1;
        offset-=80;
    }else{
        ctx.textAlign='right';
        var flip=-1;
        offset-=-10;

    }

    // set the baseline to bottom
    ctx.textBaseline='bottom';

    // set [0,0] to [x,y]
    ctx.translate(x,y);

    // rotate
    ctx.rotate(angle);

    // if the text is on the left side, flip it for readability
    ctx.scale(flip,flip);

    // draw the text
    ctx.fillText(text,offset,0);

    // always clean up!
    // reset transforms & text styling
    ctx.setTransform(1,0,0,1,0,0);
}

//выводим текст
function drawText(ctx, pos_x, pos_y, text, sizepx, color) {
  ctx.font=sizepx+" Arial";
  ctx.fillStyle = color;
  ctx.fillText(text,pos_x,pos_y);
}

//автоматическое рисование полигонов:
function poligon_dim(ctx, arr, posX, posY, line_width, line_color, pattern, dim_in_out, value){
  //nubmer of points in the poligon
var size=arr.length;


  //заливка>>>>>>>>>>>>>>>>>>>>>>
  ctx.beginPath();
  ctx.moveTo(arr[0][0]+posX,arr[0][1]+posY);
  for(var i=0;i<size;i++){
    var x1=arr[i][0]+posX;
    var y1=arr[i][1]+posY;
    var x2=arr[i][2]+posX;
    var y2=arr[i][3]+posY;
    ctx.lineTo(x2,y2);
  }
    ctx.closePath();
    ctx.lineCap = "round";
    //ctx.fillStyle="Green";

    var img = document.getElementById(pattern);
    var pat = ctx.createPattern(img, "repeat");
    ctx.fillStyle = pat;
    ctx.fill();

  //<<<<<<<<<<<<<<<<<<<<<<<<<заливка<

  for(var i=0;i<size;i++){
    var x1=arr[i][0]+posX;
    var y1=arr[i][1]+posY;
    var x2=arr[i][2]+posX;
    var y2=arr[i][3]+posY;
    drawLineDec(ctx, x1, y1, x2, y2, line_width, line_color);
    DimH(ctx, x1, y1, x2, y2, 2, "DarkBlue", dim_in_out, value);
  }

}

//полигон без размеров:
function poligon(ctx, arr, posX, posY, line_width, line_color, pattern){
  //nubmer of points in the poligon
var size=arr.length;

  //заливка>>>>>>>>>>>>>>>>>>>>>>
  ctx.beginPath();
  ctx.moveTo(arr[0][0]+posX,arr[0][1]+posY);
  for(var i=0;i<size;i++){
    var x1=arr[i][0]+posX;
    var y1=arr[i][1]+posY;
    var x2=arr[i][2]+posX;
    var y2=arr[i][3]+posY;
    ctx.lineTo(x2,y2);
  }
    ctx.closePath();
    ctx.lineCap = "round";
    //ctx.fillStyle="Green";

    var img = document.getElementById(pattern);
    var pat = ctx.createPattern(img, "repeat");
    ctx.fillStyle = pat;
    ctx.fill();

  //<<<<<<<<<<<<<<<<<<<<<<<<<заливка<

  for(var i=0;i<size;i++){
    var x1=arr[i][0]+posX;
    var y1=arr[i][1]+posY;
    var x2=arr[i][2]+posX;
    var y2=arr[i][3]+posY;
    drawLineDec(ctx, x1, y1, x2, y2, line_width, line_color);
  //  DimH(ctx, x1, y1, x2, y2, 2, "DarkBlue", dim_in_out, value);
  }

}

//прямоугольное сечение
function Rec_Dim(h,b){
  //создаем массив точек:
  var points = [
    [0, 0, b, 0],//[x1,y1,x2,y2]
    [b, 0, b, h],
    [b, h, 0, h],
    [0, h, 0, 0],
  ];
  //масштабирование вводимых значений:
  let k_norm=1;
  var flat = [];
  var flat = points.join().split(',');//сливаем все элементы points в одномерный массив flat
  var max = Math.max.apply( Math, flat );
  k_norm=1000.0/max;

  var line_width=9;
  var dim_line_width=3;
  var PosX=120; //положение полигона на канвасе
  var PosY=120;
  var line_color="Black"; //цвет контура полигона
  var dim_line_color="DarkBlue";
  var pattern="pattern3"; // образец заливки полигона из коллекции на главной странице
  var dim_in_out="out"; // внешние или внутренние линии размеров
  //var value=null; // принудительно задаваемый размер

   var points_norm = [
     [0, 0, k_norm*b, 0],//[x1,y1,x2,y2]
     [k_norm*b, 0, k_norm*b, k_norm*h],
     [k_norm*b, k_norm*h, 0, k_norm*h],
     [0, k_norm*h, 0, 0],
   ];
   poligon(ctx, points_norm, PosX, PosY, line_width,line_color, pattern);
   DimH(ctx, PosX, PosY+k_norm*h, PosX, PosY, dim_line_width, dim_line_color, dim_in_out, h);
   DimH(ctx, PosX, PosY, PosX+k_norm*b, PosY, dim_line_width, dim_line_color, dim_in_out, b);
}

//Т-сечение
function T_Dim(h0,b0,h1,b1){
  //создаем массив точек:
  var c=(b1-b0)/2;
  var points = [
    [0, 0, b1, 0],//[x1,y1,x2,y2]
    [b1, 0, b1, h1],
    [b1, h1, c+b0, h1],
    [c+b0, h1, c+b0, h0+h1],
    [c+b0, h0+h1, c, h0+h1],
    [c, h0+h1, c, h1],
    [c, h1, 0, h1],
    [0, h1, 0, 0]
  ];
  //масштабирование вводимых значений:
  let k_norm=1;
  var flat = [];
  var flat = points.join().split(',');//сливаем все элементы points в одномерный массив flat
  var max = Math.max.apply( Math, flat );
  k_norm=1000.0/max;

  var line_width=9;
  var dim_line_width=3;
  var PosX=120; //положение полигона на канвасе
  var PosY=120;
  var line_color="Black"; //цвет контура полигона
  var dim_line_color="DarkBlue";
  var pattern="pattern3"; // образец заливки полигона из коллекции на главной странице
  var dim_in_out="out"; // внешние или внутренние линии размеров
  //var value=null; // принудительно задаваемый размер

  var points_norm = [
    [0, 0, b1*k_norm, 0],//[x1,y1,x2,y2]
    [b1*k_norm, 0, b1*k_norm, h1*k_norm],
    [b1*k_norm, h1*k_norm, (c+b0)*k_norm, h1*k_norm],
    [(c+b0)*k_norm, h1*k_norm, (c+b0)*k_norm, (h0+h1)*k_norm],
    [(c+b0)*k_norm, (h0+h1)*k_norm, c*k_norm, (h0+h1)*k_norm],
    [c*k_norm, (h0+h1)*k_norm, c*k_norm, h1*k_norm],
    [c*k_norm, h1*k_norm, 0, h1*k_norm],
    [0, h1*k_norm, 0, 0]
  ];
  
   poligon(ctx, points_norm, PosX, PosY, line_width,line_color, pattern);
   DimH(ctx, PosX+(c+b0)*k_norm, PosY+(h0+h1)*k_norm, PosX+c*k_norm, PosY+(h0+h1)*k_norm, dim_line_width, dim_line_color, dim_in_out, b0);
   DimH(ctx, PosX+c*k_norm, PosY+(h0+h1)*k_norm,  PosX+c*k_norm, PosY+h1*k_norm, dim_line_width, dim_line_color, dim_in_out, h0);
   DimH(ctx, PosX, PosY+h1*k_norm,  PosX, PosY, dim_line_width, dim_line_color, dim_in_out, h1);
   DimH(ctx, PosX, PosY,  PosX+b1*k_norm, PosY, dim_line_width, dim_line_color, dim_in_out, b1);
}

//Т-сечение
function dT_Dim(h0,b0,h2,b2){
  //создаем массив точек:
  var c=(b2-b0)/2;
  var points = [
    [c, 0, c+b0, 0],//[x1,y1,x2,y2]
    [c+b0, 0, c+b0, h0],
    [c+b0, h0, b2, h0],
    [b2, h0, b2, h0+h2],
    [b2, h0+h2, 0, h0+h2],
    [0, h0+h2, 0, h0],
    [0, h0, c, h0],
    [c, h0, c, 0]
  ];
  //масштабирование вводимых значений:
  let k_norm=1;
  var flat = [];
  var flat = points.join().split(',');//сливаем все элементы points в одномерный массив flat
  var max = Math.max.apply( Math, flat );
  k_norm=1000.0/max;

  var line_width=9;
  var dim_line_width=3;
  var PosX=120; //положение полигона на канвасе
  var PosY=120;
  var line_color="Black"; //цвет контура полигона
  var dim_line_color="DarkBlue";
  var pattern="pattern3"; // образец заливки полигона из коллекции на главной странице
  var dim_in_out="out"; // внешние или внутренние линии размеров
  //var value=null; // принудительно задаваемый размер

  var points_norm = [
      [c*k_norm, 0, (c+b0)*k_norm, 0],//[x1,y1,x2,y2]
      [(c+b0)*k_norm, 0, (c+b0)*k_norm, h0*k_norm],
      [(c+b0)*k_norm, h0*k_norm, b2*k_norm, h0*k_norm],
      [b2*k_norm, h0*k_norm, b2*k_norm, (h0+h2)*k_norm],
      [b2*k_norm, (h0+h2)*k_norm, 0, (h0+h2)*k_norm],
      [0, (h0+h2)*k_norm, 0, h0*k_norm],
      [0, h0*k_norm, c*k_norm, h0*k_norm],
      [c*k_norm, h0*k_norm, c*k_norm, 0]
  ];

   poligon(ctx, points_norm, PosX, PosY, line_width,line_color, pattern);

   DimH(ctx, PosX+b2*k_norm, PosY+(h2+h0)*k_norm,  PosX, PosY+(h2+h0)*k_norm, dim_line_width, dim_line_color, dim_in_out, b2);
   DimH(ctx, PosX, PosY+(h2+h0)*k_norm,  PosX, PosY+h0*k_norm, dim_line_width, dim_line_color, dim_in_out, h2);
   DimH(ctx, PosX+c*k_norm, PosY+h0*k_norm,  PosX+c*k_norm, PosY, dim_line_width, dim_line_color, dim_in_out, h0);
   DimH(ctx, PosX+c*k_norm, PosY, PosX+(c+b0)*k_norm, PosY, dim_line_width, dim_line_color, dim_in_out, b0);
}

function udT_Dim(h0,b0,h1,b1,h2,b2){
  //создаем массив точек:
  var c1=(b1-b0)/2;
  var c2=(b2-b0)/2;
  var d=(b1-b2)/2;

  var points = [
    [0, 0, b1, 0],//[x1,y1,x2,y2]
    [b1, 0, b1, h1],
    [b1, h1, c1+b0, h1],
    [c1+b0, h1, c1+b0, h0+h1],
    [c1+b0, h0+h1, c1+b0+c2, h0+h1],
    [c1+b0+c2, h0+h1, c1+b0+c2, h0+h1+h2],
    [c1+b0+c2, h0+h1+h2, c1-c2, h0+h1+h2],//////полка b2
    [c1-c2, h0+h1+h2, c1-c2, h0+h1],///полка h2
    [c1-c2, h0+h1, c1, h0+h1],
    [c1, h0+h1, c1, h1],
    [c1, h1, 0, h1],
    [0, h1, 0, 0]
  ];
  //масштабирование вводимых значений:
  let k_norm=1;
  var flat = [];
  var flat = points.join().split(',');//сливаем все элементы points в одномерный массив flat
  var max = Math.max.apply( Math, flat );
  k_norm=1000.0/max;


  var line_width=9;
  var dim_line_width=3;
  var PosX=120; //положение полигона на канвасе
  var PosY=120;
  var line_color="Black"; //цвет контура полигона
  var dim_line_color="DarkBlue";
  var pattern="pattern3"; // образец заливки полигона из коллекции на главной странице
  var dim_in_out="out"; // внешние или внутренние линии размеров
  //var value=null; // принудительно задаваемый размер

  var points_norm = [
    [0, 0, b1*k_norm, 0],//[x1,y1,x2,y2]
    [b1*k_norm, 0, b1*k_norm, h1*k_norm],
    [b1*k_norm, h1*k_norm, (c1+b0)*k_norm, h1*k_norm],
    [(c1+b0)*k_norm, h1*k_norm, (c1+b0)*k_norm, (h0+h1)*k_norm],
    [(c1+b0)*k_norm, (h0+h1)*k_norm, (c1+b0+c2)*k_norm, (h0+h1)*k_norm],
    [(c1+b0+c2)*k_norm, (h0+h1)*k_norm, (c1+b0+c2)*k_norm, (h0+h1+h2)*k_norm],
    [(c1+b0+c2)*k_norm, (h0+h1+h2)*k_norm, (c1-c2)*k_norm, (h0+h1+h2)*k_norm],//////полка b2
    [(c1-c2)*k_norm, (h0+h1+h2)*k_norm, (c1-c2)*k_norm, (h0+h1)*k_norm],///полка h2
    [(c1-c2)*k_norm, (h0+h1)*k_norm, c1*k_norm, (h0+h1)*k_norm],
    [c1*k_norm, (h0+h1)*k_norm, c1*k_norm, h1*k_norm],
    [c1*k_norm, h1*k_norm, 0, h1*k_norm],
    [0, h1*k_norm, 0, 0]
  ];

   PosX=PosX+Math.abs(d)+50;
   poligon(ctx, points_norm, PosX, PosY, line_width,line_color, pattern);



    DimH(ctx, PosX+(c1+b0+c2)*k_norm, PosY+(h0+h1+h2)*k_norm,  PosX+(c1-c2)*k_norm, PosY+(h0+h1+h2)*k_norm, dim_line_width, dim_line_color, dim_in_out, b2);
    DimH(ctx, PosX+(c1-c2)*k_norm, PosY+(h0+h1+h2)*k_norm,  PosX+(c1-c2)*k_norm, PosY+(h1+h0)*k_norm, dim_line_width, dim_line_color, dim_in_out, h2);
    DimH(ctx, PosX+c1*k_norm, PosY+(h0+h1)*k_norm,  PosX+c1*k_norm, PosY+h1*k_norm, dim_line_width, dim_line_color, dim_in_out, h0);
    DimH(ctx, PosX, PosY+h1*k_norm,  PosX, PosY, dim_line_width, dim_line_color, dim_in_out, h1);
    DimH(ctx, PosX+c1*k_norm, PosY+(h1+h0)*k_norm, PosX+(c1+b0)*k_norm, PosY+(h1+h0)*k_norm, dim_line_width, dim_line_color, dim_in_out, b0);
    DimH(ctx, PosX, PosY,  PosX+b1*k_norm, PosY, dim_line_width, dim_line_color, dim_in_out, b1);
}

function Title(){
  drawFace(ctx, 1200,1200);
  // drawLineAng(ctx, 50, 0, 90, 500, 10, "Red");
  // drawLineDec(ctx, 50, 50, 550, 50, 10, "Yellow");
  drawLineDec(ctx, 150, 50, 800, 50, 1, "White");
  drawText(ctx, 250, 100, "Заданное сечение:", "50px", "White");
}
