var method_selection;//переменная текущего выбора сечения

  $(function(){
    $('.item-1').click(function(){
      Cookies.set('slide_selection', 1); // перменная текущего выбора слайда
      $(".item-1-img").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
      $('html').animate({
        scrollTop: $("#cross-sections").offset().top// Scroll screen to target element
      }, 200);
      $(".item-2").load(" .item-2");//перегружаем соседние окна, чтобы обновить куки
      $(".item-3").load(" .item-3");
      $(".item-4").load(" .item-4");
    });
    $('.item-2').click(function(){
      Cookies.set('slide_selection', 2); // перменная текущего выбора слайда
      $(".item-2-img").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
      $('html').animate({
        scrollTop: $("#cross-sections").offset().top// Scroll screen to target element
      }, 200);
      $(".item-1").load(" .item-1");//перегружаем соседние окна, чтобы обновить куки
      $(".item-3").load(" .item-3");
      $(".item-4").load(" .item-4");
    });
    $('.item-3').click(function(){
      Cookies.set('slide_selection', 3); // перменная текущего выбора слайда
      $(".item-3-img").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
      $('html').animate({
        scrollTop: $("#cross-sections").offset().top// Scroll screen to target element
      }, 200);
      $(".item-1").load(" .item-1");//перегружаем соседние окна, чтобы обновить куки
      $(".item-2").load(" .item-2");
      $(".item-4").load(" .item-4");
    });
    $('.item-4').click(function(){
      Cookies.set('slide_selection', 4); // перменная текущего выбора слайда
      $(".item-4-img").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
      $('html').animate({
        scrollTop: $("#cross-sections").offset().top// Scroll screen to target element
      }, 200);
      $(".item-1").load(" .item-1");//перегружаем соседние окна, чтобы обновить куки
      $(".item-2").load(" .item-2");
      $(".item-3").load(" .item-3");
    });
  });

  $(function() {
    method_selection=Cookies.get('slide_selection');
          if(method_selection==1){
               $(".item-1-img").css('box-shadow', '10px 10px 28px #6495ED');
          }
          if(method_selection==2){
               $(".item-2-img").css('box-shadow', '10px 10px 28px #6495ED');
          }
          if(method_selection==3){
               $(".item-3-img").css('box-shadow', '10px 10px 28px #6495ED');
          }
          if(method_selection==4){
               $(".item-4-img").css('box-shadow', '10px 10px 28px #6495ED');
          }

      });
