var cross_section_selection;//переменная текущего выбора сечения
var val_h = [300,100,100]// массивы введенных значений параметров сечения
var val_b = [100,300,300];
var val_b_buf = [300,100,100];
var val_h_buf = [100,300,300];
        $(function(){

            $(".cross-section-item-1").click(function(){
              Cookies.set('cross_selection', 1); // перменная текущего выбора сечения
              $(".item-1-cross").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
              $('html').animate({
                scrollTop: $("#cross-parameters").offset().top// Scroll screen to target element
              }, 200);
              $("#cross-parameters").html("");//очищаем ячейку грида
              cross_section_selector("h","b",0,0,0,0,cross_selection);//вписываем в ячейку параметры и отрисовываем сечение
              $(".cross-section-item-2").load(" .cross-section-item-2");//перегружаем соседние окна, чтобы обновить куки
              $(".cross-section-item-3").load(" .cross-section-item-3");
              $(".cross-section-item-4").load(" .cross-section-item-4");
              ctx.clearRect(0, 0, canvas.width, canvas.height);
            });

            $('.cross-section-item-2').click(function(){
              Cookies.set('cross_selection', 2); // перменная текущего выбора сечения
              $(".item-2-cross").css('box-shadow', '10px 10px 28px#6495ED');//выделяем выбранную картинку
              $('html').animate({
                scrollTop: $("#cross-parameters").offset().top// Scroll screen to target element
              }, 200);
              $("#cross-parameters").html("");//очищаем ячейку грида
              cross_section_selector("h","b","h1","b1",0,0);//вписываем в ячейку параметры
              $(".cross-section-item-1").load(" .cross-section-item-1");
              $(".cross-section-item-3").load(" .cross-section-item-3");
              $(".cross-section-item-4").load(" .cross-section-item-4");
              ctx.clearRect(0, 0, canvas.width, canvas.height);
            });

            $('.cross-section-item-3').click(function(){
              Cookies.set('cross_selection', 3); // перменная текущего выбора сечения
              $(".item-3-cross").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
              $('html').animate({
                scrollTop: $("#cross-parameters").offset().top// Scroll screen to target element
              }, 200);
              $("#cross-parameters").html("");//очищаем ячейку грида
              cross_section_selector("h","b",0,0,"h2","b2");//вписываем в ячейку параметры
              $(".cross-section-item-1").load(" .cross-section-item-1");//перегружаем соседние окна, чтобы обновить куки
              $(".cross-section-item-2").load(" .cross-section-item-2");
              $(".cross-section-item-4").load(" .cross-section-item-4");
              ctx.clearRect(0, 0, canvas.width, canvas.height);
            });

            $('.cross-section-item-4').click(function(){
              Cookies.set('cross_selection', 4); // перменная текущего выбора сечения
              $(".item-4-cross").css('box-shadow', '10px 10px 28px #6495ED');//выделяем выбранную картинку
              $('html').animate({
                scrollTop: $("#cross-parameters").offset().top// Scroll screen to target element
              }, 200);
              $("#cross-parameters").html("");//очищаем ячейку грида
              cross_section_selector("h","b","h1","b1","h2","b2");//вписываем в ячейку параметры
              $(".cross-section-item-1").load(" .cross-section-item-1");//перегружаем соседние окна, чтобы обновить куки
              $(".cross-section-item-2").load(" .cross-section-item-2");
              $(".cross-section-item-3").load(" .cross-section-item-3");
              ctx.clearRect(0, 0, canvas.width, canvas.height);
            });

        });


            $(function() {
              cross_section_selection=Cookies.get('cross_selection');
                    if(cross_section_selection==1){
                         $(".item-1-cross").css('box-shadow', '10px 10px 28px #6495ED');
                    }
                    if(cross_section_selection==2){
                         $(".item-2-cross").css('box-shadow', '10px 10px 28px #6495ED');
                    }
                    if(cross_section_selection==3){
                         $(".item-3-cross").css('box-shadow', '10px 10px 28px #6495ED');
                    }
                    if(cross_section_selection==4){
                         $(".item-4-cross").css('box-shadow', '10px 10px 28px #6495ED');
                    }

                });


//функция автозагрузки страницы с выбранными ранее параметрами
    $(function(){
      cross_selection=Cookies.get('cross_selection');

      if (cross_selection==1) {cross_section_selector("h","b",0,0,0,0);
        h0=parseInt(Cookies.get('h0'));
        b0=parseInt(Cookies.get('b0'));
        if (h0==undefined) h0=300;
        if (b0==undefined) b0=300;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        Rec_Dim(b0,h0);
      }
        if (cross_selection==2) {cross_section_selector("h","b","h1","b1",0,0);
          h0=parseInt(Cookies.get('h0'));
          b0=parseInt(Cookies.get('b0'));
          h1=parseInt(Cookies.get('h1'));
          b1=parseInt(Cookies.get('b1'));
          if (h0==undefined) h0=300;
          if (b0==undefined) b0=100;
          if (h1==undefined) h1=100;
          if (b1==undefined) b1=300;
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          if (b0>b1) b0=b1;
          T_Dim(h0,b0,h1,b1);
      }
      if (cross_selection==3) {cross_section_selector("h","b",0,0,"h2","b2");
        h0=parseInt(Cookies.get('h0'));
        b0=parseInt(Cookies.get('b0'));
        h2=parseInt(Cookies.get('h2'));
        b2=parseInt(Cookies.get('b2'));
        if (h0==undefined) h0=300;
        if (b0==undefined) b0=100;
        if (h2==undefined) h2=100;
        if (b2==undefined) b2=300;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        if (b0>b2) b0=b2;
        dT_Dim(h0,b0,h2,b2);
      }

      if (cross_selection==4) {cross_section_selector("h","b","h1","b1","h2","b2");
      h0=parseInt(Cookies.get('h0'));
      b0=parseInt(Cookies.get('b0'));
      h1=parseInt(Cookies.get('h1'));
      b1=parseInt(Cookies.get('b1'));
      h2=parseInt(Cookies.get('h2'));
      b2=parseInt(Cookies.get('b2'));
      if (h0==undefined) h0=300;
      if (b0==undefined) b0=100;
      if (h1==undefined) h1=100;
      if (b1==undefined) b1=300;
      if (h2==undefined) h2=100;
      if (b2==undefined) b2=300;
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      udT_Dim(h0,b0,h1,b1,h2,b2);
    }
    });

    function cross_section_selector(h,b,h1,b1,h2,b2){
          var arrH=[]; // массив выбранных параметров высот
          var arrB=[];

          if(h!="")arrH.push(h);
          if(h1!="")arrH.push(h1);
          if(h2!="")arrH.push(h2);

          if(b!="")arrB.push(b);
          if(b1!="")arrB.push(b1);
          if(b2!="")arrB.push(b2);

        var elementToPaste =  $("#cross-parameters");
        var classToAppend="";

          for(var i=0;i<arrH.length;i++){//hi, i=from 1 to 3
            classToAppend="";
            if(arrH[i]==h)classToAppend="rightMirgin";
            //draw hi inputs
          elementToPaste.append('\
          <div class="form-group">\
          <label class="param '+classToAppend+'" for="'+arrH[i]+'">'+arrH[i]+':</label>\
           <input class="param form-control" type="text" id="'+arrH[i]+'">\
            <select class="param form-control" id="sel'+arrH[i]+'">\
               <option value=1>мм</option>\
               <option value=10>см</option>\
               <option value=1000>м</option>\
             </select>\
          </div>')
          }
          for(var i=0;i<arrB.length;i++){//bi, i=from 1 to 3
            classToAppend="";
            if(arrB[i]==b)classToAppend="rightMirgin";
            //draw bi inputs
            elementToPaste.append("\
            <div class='form-group'>\
            <label class='param "+classToAppend+"' for='"+arrB[i]+"'>"+arrB[i]+":</label>\
             <input class='param form-control' type='text' id='"+arrB[i]+"'>\
              <select class='param form-control' id='sel"+arrB[i]+"'>\
                 <option value=1>мм</option>\
                 <option value=10>см</option>\
                 <option value=1000>м</option>\
               </select>\
            </div>");
          }
        read_input(arrB,arrH); //считываем инпуты и селекторы параметров сечений в реальном времени
        }

function onArrBInputCallback(arrB,i){//функция возвращающая функцию для возможности реализации функции онклик в цикле
  return function() {
     var dim_koeff=$('#sel'+arrB[i]+' option:selected').val();//считываем селектор единиц измерения
     val_b[i] = $.trim($('#'+arrB[i]).val())*dim_koeff;//и переводим значение параметра в миллиметры
     val_b_buf[i]=val_b[i];
     // ctx.clearRect(0, 0, canvas.width, canvas.height);
     cross_selection=Cookies.get('cross_selection');

     if (cross_selection==1) {//рисуем прямоугольник
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       Rec_Dim(parseInt(val_b[0]),parseInt(val_h[0]));
       if (i==0) Cookies.set('b0', val_b[0]);
     }
     if (cross_selection==2) {//рисуем верхний тавр
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       if (val_b[0]>val_b[1]){
//защита от некорректного ввода
      val_b[0]=val_b[1];
     }
     //взвращаем последствия защиты от неправильн. ввода:
     if(val_b_buf[0]<val_b[1])
     {
       val_b[0]=val_b_buf[0];
     }

       T_Dim(parseInt(val_h[0]),parseInt(val_b[0]),parseInt(val_h[1]),parseInt(val_b[1]));
       if (i==0) Cookies.set('b0', val_b[0]); // перменная текущего параметра сечения
       if (i==1) Cookies.set('b1', val_b[1]);
     }
     if (cross_selection==3) {//рисуем нижний тавр
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       if (val_b[0]>val_b[1]) val_b[0]=val_b[1];//защита от некорректного ввода
       //взвращаем последствия защиты от неправильн. ввода:
       if(val_b_buf[0]<val_b[1])
       {
         val_b[0]=val_b_buf[0];
       }
       
       dT_Dim(parseInt(val_h[0]),parseInt(val_b[0]),parseInt(val_h[1]),parseInt(val_b[1]));
       if (i==0) Cookies.set('b0', val_b[0]); // перменная текущего параметра сечения
       if (i==1) Cookies.set('b2', val_b[1]);
     }
     if (cross_selection==4) {//рисуем нижний тавр
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       udT_Dim(parseInt(val_h[0]),parseInt(val_b[0]),parseInt(val_h[1]),parseInt(val_b[1]),parseInt(val_h[2]),parseInt(val_b[2]));
       if (i==0) Cookies.set('b0', val_b[0]); // перменная текущего параметра сечения
       if (i==1) Cookies.set('b1', val_b[1]);
       if (i==2) Cookies.set('b2', val_b[2]);
     }
     console.log("val_b["+i+"]="+val_b[i]);
     }
}

function onArrHInputCallback(arrH,i){
  return function() {
     var dim_koeff=$('#sel'+arrH[i]+' option:selected').val();//считываем селектор единиц измерения
     var my_val=$.trim($('#'+arrH[i]).val());
     val_h[i] = my_val*dim_koeff;//и переводим значение параметра в миллиметры

     if (cross_selection==1) {//рисуем прямоугольник
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       Rec_Dim(parseInt(val_b[0]),parseInt(val_h[0]));
       if (i==0) Cookies.set('h0', val_h[0]); // перменная текущего параметра сечения
     }
     if (cross_selection==2) {//рисуем верхний тавр
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       T_Dim(parseInt(val_h[0]),parseInt(val_b[0]),parseInt(val_h[1]),parseInt(val_b[1]));
       if (i==0) Cookies.set('h0', val_h[0]); // перменная текущего параметра сечения
       if (i==1) Cookies.set('h1', val_h[1]);
     }
     if (cross_selection==3) {//рисуем нижний тавр
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       dT_Dim(parseInt(val_h[0]),parseInt(val_b[0]),parseInt(val_h[1]),parseInt(val_b[1]));
       if (i==0) Cookies.set('h0', val_h[0]); // перменная текущего параметра сечения
       if (i==1) Cookies.set('h2', val_h[1]);
     }
     if (cross_selection==4) {//рисуем нижний тавр
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       udT_Dim(parseInt(val_h[0]),parseInt(val_b[0]),parseInt(val_h[1]),parseInt(val_b[1]),parseInt(val_h[2]),parseInt(val_b[2]));
       if (i==0) Cookies.set('h0', val_h[0]); // перменная текущего параметра сечения
       if (i==1) Cookies.set('h1', val_h[1]);
       if (i==2) Cookies.set('h2', val_h[2]);
     }
     console.log("val_h["+i+"]="+val_h[i]);
  }
}

function read_input(arrB,arrH){//считываем инпуты и селекторы параметров сечений в реальном времени
  $(document).ready(function() {
    for(var i=0;i<arrB.length;i++){//bi, i=from 1 to 3
      $('#sel'+arrB[i]).on('click', onArrBInputCallback(arrB,i));
      $('#'+arrB[i]).on('input', onArrBInputCallback(arrB,i));
    }
    for(var i=0;i<arrH.length;i++){//hi, i=from 1 to 3
      $('#sel'+arrH[i]).on('click',  onArrHInputCallback(arrH,i));
      $('#'+arrH[i]).on('input', onArrHInputCallback(arrH,i));
    }
  });
}
