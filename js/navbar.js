/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-50px";
  }
  prevScrollpos = currentScrollPos;
}

// change active nav item:
// $(".nav a").on("click", function(){
//    // $(".nav").find(".active").removeClass("active");
//    $(this).parent().addClass("active");
//    // load new page:
//    if($(this).parent().hasClass("index")){
//      //hide my-title from index.html:
//    $(".index-title").hide();
//    //load page-1.html:
//    // $("#main").load("index.html");
//       }else{
//             if($(this).parent().hasClass("page-1")){
//               //hide my-title from index.html:
//                  $(".index-title").hide();
//                  //load page-2.html:
//                  // $("#main").load("pages/page-1.html");
//             }
//         }
// });
